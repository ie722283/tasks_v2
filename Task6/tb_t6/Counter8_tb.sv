
module Counter8_tb;

// Inputs
logic clk;
logic enb;
logic rst;
logic sync_rst;
logic [3:0]count5_i;

// Outputs
logic [3:0]count8_o;
logic ovf8;
logic parity_o;

 // Instantiate UUT
Counter_8 uut(
	.clk(clk),
	.enb(enb),
	.sync_rst(sync_rst),
	.rst(rst),
	.count5_i(count5_i),
	
	.count8_o(count8_o), 
	.ovf8(ovf8),
	.parity_o(parity_o)
);

// Initialize signals
 initial begin
	  clk = 0;
	  rst = 0;
	  enb = 0;
	  sync_rst = 0;
	  count5_i = 0;

	  // Toggle reset signal
	#2 rst = 0;
	#2 rst = 1;
	#2 enb = 1;
	#2 count5_i = 2;//1
	#2 count5_i = 0;
	#2 count5_i = 2;//2
	#2 count5_i = 0;	
	#2 count5_i = 2;//3	
	#2 count5_i = 0;
	#2 count5_i = 2;//4
	#2 count5_i = 0;
	#2 count5_i = 2;//5
	#2 count5_i = 0;
	#2 count5_i = 2;//6
	#2 count5_i = 0;
	#2 count5_i = 2;	//7
	#2 count5_i = 0;
	#2 count5_i = 2;//8
	#2 count5_i = 0;
	#2 count5_i = 2;//9
	#2 count5_i = 0;
	#2 count5_i = 2;//0
	#2 count5_i = 0;
	#2 count5_i = 0;//1	
	

	  // Run simulation for certain duration
	  #200;

	  // Stop simulation
	  $stop;
 end

// Generate clock signal
always begin
    #1 clk = ~clk;
end



endmodule 


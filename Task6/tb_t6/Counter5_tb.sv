
module Counter5_tb;

// Inputs
logic clk;
logic enb;
logic rst;
logic sync_rst;

// Outputs
logic [2:0] count5_o;
logic ovf5;

 // Instantiate UUT
Counter_5 uut(
	.clk(clk),
	.enb(enb),
	.sync_rst(sync_rst),
	.rst(rst),
	
	.count5_o(count5_o), 
	.ovf5(ovf5)
);

// Initialize signals
 initial begin
	  clk = 0;
	  rst = 0;
	  enb = 0;
	  sync_rst = 0;

	  // Toggle reset signal
	#2 rst = 0;
	#4 rst = 1;
	#6 enb = 1;

	  // Run simulation for certain duration
	  #200;

	  // Stop simulation
	  $stop;
 end

// Generate clock signal
always begin
    #1 clk = ~clk;
end



endmodule 


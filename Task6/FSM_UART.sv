// Project: UART Rx
// Module:  FSM
// Author:  María Luisa Ortiz Carstensen
// Date:    23 febrero 2023

module FSM_UART #(parameter DW=8)
(
	input 	clk,
	input 	rst, 
	input 	ovf8,
	input 	ovf5,
	input 	enb,
	input 	rx, 
	input 	parity,
	input 	[DW-5:0]count5_i,
	input		[DW-5:0]count8_i,
	
	output 	rx_done,
	output	data,
	output	rx_out,
	output	syncrst
);

typedef enum logic[2:0]{
	IDLE		= 3'b000,
	START		= 3'b001,
	DATA		= 3'b010,
	PARITY	= 3'b011,
	DONE		= 3'b100
} fsm_type;

fsm_type current_state;
fsm_type next_state;

logic rx_out_l;
logic done_l;
logic data_l;
logic syncrst_l;

always_ff @(posedge clk, negedge rst) begin

	if(!rst)
		current_state <= IDLE;
	else
		current_state <= next_state;
		
end


always_comb begin //Conditions to change state
	case(current_state)
		IDLE: begin //IDLE 
			if(rx == 0) //Start bit is 0, so start bit was received
				next_state = START; 
			else 
				next_state = IDLE;
		end
		START:begin
			if(count8_i == 1)
				next_state = DATA; //Done receiving first bit, each one lasts 5 clk cycles
			else
				next_state = START;
		end
		DATA: begin 
			if(ovf8 == 1)
				next_state = PARITY;
			else 
				next_state = DATA;
		end
		PARITY:begin
			if(parity)
				next_state = DONE;
			else 
				next_state = PARITY;
		end
		
		DONE:begin
				next_state = IDLE;
		end
		
		default: begin
			next_state = IDLE;
		end
	endcase
end


always_comb begin // Outputs

	case(current_state)
	
		IDLE: begin
			syncrst_l		= 1'b1;
			done_l			= 1'b0;
			data_l			= 1'b0;			
		end
		
		START: begin
			syncrst_l		= 1'b0;
			done_l			= 1'b0;
			data_l			= 1'b0;			
		end
		
		DATA: begin
			syncrst_l		= 1'b0;
			done_l			= 1'b0;
			data_l			= 1'b1;			
		end
		
		PARITY: begin
			syncrst_l		= 1'b0;
			done_l			= 1'b0;
			data_l			= 1'b0;			
		end
		
		DONE: begin
			syncrst_l		= 1'b0;
			done_l			= 1'b1;
			data_l			= 1'b0;	
		end
	
	endcase
	
end	

assign rx_done		= done_l;
assign data			= data_l;
assign syncrst		= syncrst_l;



endmodule 

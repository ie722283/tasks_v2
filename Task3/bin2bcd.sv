// Add 3 and shift left algorithm
// Double Dabble

module bin2bcd
(
	input  [7:0]bin_deco,
	output [11:0]bcd_deco
);

// Module 1 
wire mod1_out3;
wire mod1_out2;
wire mod1_out1;
wire mod1_out0;

// Module 2
wire mod2_out3;
wire mod2_out2;
wire mod2_out1;
wire mod2_out0;

// Module 3
wire mod3_out3;
wire mod3_out2;
wire mod3_out1;
wire mod3_out0;

// Module 4
wire mod4_out3;
wire mod4_out2;
wire mod4_out1;
wire mod4_out0;

// Module 5
wire mod5_out3;
wire mod5_out2;
wire mod5_out1;
wire mod5_out0;

// Module 6
wire mod6_out3;
wire mod6_out2;
wire mod6_out1;
wire mod6_out0;

// Module 7
wire mod7_out3;
wire mod7_out2;
wire mod7_out1;
wire mod7_out0;

Add_three mod1 
(
	.inp_add({1'b0, bin_deco[7:5]}),
	.out_add({mod1_out3, mod1_out2, mod1_out1, mod1_out0})
); 

Add_three mod2 
(
	.inp_add({mod1_out2,mod1_out1,mod1_out0,bin_deco[4]}),
	.out_add({mod2_out3, mod2_out2, mod2_out1, mod2_out0})
); 

Add_three mod3 
(
	.inp_add({mod2_out2,mod2_out1,mod2_out0,bin_deco[3]}),
	.out_add({mod3_out3, mod3_out2, mod3_out1, mod3_out0})
); 

Add_three mod4 
(
	.inp_add({mod3_out2,mod3_out1,mod3_out0,bin_deco[2]}),
	.out_add({mod4_out3, mod4_out2, mod4_out1, mod4_out0})
); 

Add_three mod5 
(
	.inp_add({mod4_out2,mod4_out1,mod4_out0,bin_deco[1]}),
	.out_add({mod5_out3, mod5_out2, mod5_out1, mod5_out0})
); 

Add_three mod6 
(
	.inp_add({1'b0,mod1_out3,mod2_out3,mod3_out3}),
	.out_add({mod6_out3, mod6_out2, mod6_out1, mod6_out0})
); 

Add_three mod7 
(
	.inp_add({mod6_out2, mod6_out1, mod6_out0, mod4_out3}),
	.out_add({mod7_out3, mod7_out2, mod7_out1, mod7_out0})
); 

assign bcd_deco[11:8] = {1'b0,1'b0, mod6_out3, mod7_out3};//Hundreds
assign bcd_deco[7:4]  = {mod7_out2, mod7_out1, mod7_out0, mod5_out3};//Tens
assign bcd_deco[3:0]  = {mod5_out2, mod5_out1, mod5_out0, bin_deco[0]};//Ones

endmodule



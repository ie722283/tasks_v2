//Decimal to segments

module dec2seg
(
	input  [3:0]bcd_dec2seg,
	output [6:0]segments 
);

 always_comb begin
		case(bcd_dec2seg)
		0: segments = 7'b1000000;
		1: segments = 7'b1111001;
		2: segments = 7'b0100100;
		3: segments = 7'b0110000;
		4: segments = 7'b0011001;
		5: segments = 7'b0010010;
		6: segments = 7'b0000010;
		7: segments = 7'b0111000;
		8: segments = 7'b0000000;
		9: segments = 7'b0011000;
		default:segments = 7'b1111111;
		endcase

 end


endmodule 
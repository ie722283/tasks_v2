



module Fibonacci_Beh #(parameter DW = 10)
(
    input clk,
    input rst,
    output [DW-1:0] fib
);


reg [DW-1:0] fib0 = 0;   // Primer número
reg [DW-1:0] fib1 = 1;   // Segundo número
reg [DW-1:0] next_fib;   // Siguiente número

always_ff @(posedge clk or negedge rst) begin
    if (!rst) begin
        // Si está en estado de reset, reinicia la serie de Fibonacci
        fib <= 0;
        fib0 <= 0;
        fib1 <= 1;
    end 
	 else begin
        next_fib = fib0 + fib1;// Calcula el siguiente número de Fibonacci sumando los dos últimos
        fib0 <= fib1; // Mueve los valores hacia adelante para el siguiente ciclo
        fib1 <= next_fib;
        fib <= next_fib;// Actualiza el valor de salida
    end
end

endmodule